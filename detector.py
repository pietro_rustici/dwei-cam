import tensorflow.keras
from PIL import Image, ImageOps
import numpy as np
import cv2
import time
import argparse
import requests

# function to determine if 3 seconds are passed from the last interference
def watchdog(oldepoch):
    return int(time.time() * 1000) - oldepoch >= 3000

# Setup the arguments
parser = argparse.ArgumentParser(description='Runs the ML algorithm aganist the webcam video feed.')
parser.add_argument('-ip', help='The IP of the ESP32.')

args = parser.parse_args()
if not (args.ip):
    parser.error('Please specify the ip address using -ip "xxx.xxx.xxx.xxx"')
ip = args.ip
server = 'http://' + ip + '/status/close?'

print("Connecting to " + ip + " ...")

# Set the input source
vid = cv2.VideoCapture(0) 
# Load the. model
np.set_printoptions(suppress=True)
model = tensorflow.keras.models.load_model('model/keras_model.h5')
# The variable to pass the image to TF
data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)
size = (224, 224)
oldepoch = 0;

while(True):
    # Keep reading the buffer otherwise it piles up
    ret, frame = vid.read()
    if(ret and watchdog(oldepoch)):
        # if we have a valid frame from the webcam
        # and the timer exceeded 3 seconds
        # convert the image to a openvc compatible one
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = Image.fromarray(frame)
        # Fit the image to the keras box
        image = ImageOps.fit(frame, size, Image.ANTIALIAS)
        image_array = np.asarray(image)
        normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1
        data[0] = normalized_image_array

        # run the inference
        prediction = model.predict(data)
        
        if(prediction[0][0] > 0.8):
            print("present")
        else:
            print("not present")
            r = requests.get(server)
            if(r.status_code == 200):
                print("Signal sent")
            else:
                print("Error delivering the message to " + ip)
            time.sleep(2)
        oldepoch = int(time.time() * 1000)

