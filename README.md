# DwEI-cam
Design with Extended Intelligence Team 4 -> machine / MDEF 2020/21
![alt text](images/state-machine.png "State Machine")
### Concept
 A gizmo that detects when your pose in front of the camera doesn't suit the online call's etiquette. The system is composed of an app that runs on the host computer, which listens to the video feed of the webcam and classifies the pose using a machine learning algorithm. The state change is then communicated through the HTTP protocol to the widget hanging on top of the laptop's pinhole camera. Next, the device shutters the camera by moving a tiny hand in front of it.

### Installation - app

 1. Clone this repository.    
 2. Navigate to the folder using the bash shell.
 3. Create a new virtual environment
		
		python3 -m venv venv
		
 4. Activate the environment (Unix/Mac OS)
 		
 		source venv/bin/activate
    
 5. Install the python's dependencies
 
		pip3 install -r requirements.txt

 6. Run the application

		python3 detector.py -ip "xxx.xxx.xxx.xxx"
